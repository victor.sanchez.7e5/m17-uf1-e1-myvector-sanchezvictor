﻿using System;


namespace M17_UF1_E1_MyVector_SanchezVictor
{
    class MyVector
    {
        //private double x1 = {get;set;}; 

        //punt1
        private double _x0;
        private double _y0;

        //punt2
        private double _x1;
        private double _y1; 
        

        private MyVector(double x0 , double y0 , double x1 , double y1)
        {

            this._x0 = x0;
            this._y0 = y0;
            this._x1 = x1;
            this._y1 = y1; 

        }

        public MyVector()
        {
        }

        public void SetX0 (double x0)
        {
            _x0 = x0; 
        }
        public double Getx0()
        {
            return _x0;
        }

        public void SetY0(double y0)
        {
            _y0 = y0;
        }
        public double Gety0()
        {
            return _y0;
        }

        public void SetX1(double x1)
        {
            _x1 = x1;
        }
        public double Getx1()
        {
            return _x1;
        }
        public void SetY1(double y1)
        {
            _y1 = y1;
        }
        public double Gety1()
        {
            return _y1;
        }



        public void creaVectors()
        {

            double[] punt1 = new double[] { _x0, _y0 };
            double[] punt2 = new double[] { _x1, _y1 };

        }
            
        public void distancia()
        {

            double modulo = Math.Sqrt(Math.Pow((_x1 - _x0), 2) + Math.Pow((_y1 - _y0),2)) ;

            Console.WriteLine("La distancia del vector és de " + modulo + " u.");

        }

        public void canviaDir()
        {



        }

        public override string ToString() => $"El Vector és creat amb el punt A :  , i amb el punt B : \n\r " +
            $"I la distància o mòdul és = . ";

        /*Guarda i gestiona vectors de dos dimensions
          -Variable on es guarda les coordenades 2D d’un vector. Punt d’inici i punt final.
Funcions get i set 
Funcions que retornen la distancia del vector
Funció que canvia la direcció del vector
Funció .toString()*/

    }
}

 